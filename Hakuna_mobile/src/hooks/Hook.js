import { createRef, useEffect } from "react";
import { useTranslation } from "react-i18next";

const hookRef = createRef();

function Hooks() {

    const { t, i18n } = useTranslation();

    useEffect(() => {
        hookRef.current = {
            t: t,
            i18n: i18n
        }
    }, []);

    useEffect(() => {
        hookRef.current = {
            t: t,
            i18n: i18n
        }
    }, [t, i18n]);

    return (
        <>
        </>
    );
}

const useTranslations = () => {
    return ({
        t: hookRef.current.t,
        i18n: hookRef.current.i18n
    });
}

export default Hooks
export {
    useTranslations as useTranslation
}