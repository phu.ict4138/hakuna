import Hooks from "./Hook";

import { useTranslation } from "./Hook";
export {
    Hooks,
    useTranslation
}

const Hook = {
    useTranslation: useTranslation
}

export default Hook
