import { useNavigation } from "@react-navigation/native";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Color from "../../globals/Color";
import { iconCancel } from "../Icon";



function Header({ title, goBack, rightComponent }) {

    const navigation = useNavigation();

    return (
        <View style={styles.wrapper}>
            <TouchableOpacity
                style={styles.back}
                onPress={() => {
                    if (goBack) {
                        navigation.goBack();
                    }
                }}>
                {goBack &&
                    <Image
                        style={styles.iconCancel}
                        resizeMode={"contain"}
                        source={iconCancel} />
                }
            </TouchableOpacity>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.right}>
                {rightComponent}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        height: 60,
        flexDirection: "row",
        alignItems: "center",
        borderBottomColor: Color.gray,
        borderBottomWidth: 0.2
    },
    back: {
        position: "absolute",
        top: 0,
        left: 0,
        height: 60,
        justifyContent: "center",
        zIndex: 1
    },
    iconCancel: {
        width: 20,
        height: 20,
        marginLeft: 16
    },
    title: {
        flex: 1,
        textAlign: "center",
        fontSize: 16
    },
    right: {
        position: "absolute",
        top: 0,
        right: 0,
        height: 60,
        alignItems: "center",
        justifyContent: "center"
    }
});


export default Header