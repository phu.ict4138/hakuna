import { Image, Pressable, StyleSheet, Text, View } from "react-native";
import Color from "../../globals/Color";
import Const from "../../globals/Const";

function FollowerRow({ name, avatar, crown, componentRight, onPress }) {

    return (
        <Pressable
            style={({ pressed }) => ({
                ...styles.wrapper,
                backgroundColor: pressed ? Color._f7f7f7 : Color._ffffff
            })}
            onPress={onPress}>
            <View style={styles.avatar}>
                <Image
                    style={styles.image}
                    resizeMode={"contain"}
                    source={{ uri: avatar }} />
                <View style={styles.border}>
                    <Image
                        style={styles.crown}
                        resizeMode={"contain"}
                        source={{ uri: crown }} />
                </View>
            </View>
            <Text style={styles.name}>{name}</Text>
        </Pressable>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        height: Const.const_80,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: Const.const_16
    },
    avatar: {
        marginRight: Const.const_12
    },
    image: {
        width: Const.const_50,
        height: Const.const_50,
        borderRadius: 99,
        borderWidth: Const.const_1,
        borderColor: Color._515151
    },
    border: {
        position: "absolute",
        top: Const.const_0,
        bottom: Const.const_0,
        right: Const.const_0,
        left: Const.const_0,
        alignItems: "center",
        justifyContent: "center"
    },
    crown: {
        width: Const.const_72,
        height: Const.const_72,
    },
    name: {
        flex: 1
    },
    right: {

    }
});

export default FollowerRow