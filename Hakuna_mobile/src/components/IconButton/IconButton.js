import { Image, TouchableOpacity } from "react-native"

function IconButton({ icon, size = 32, style, color, onPress }) {

    return (
        <TouchableOpacity
            style={[
                {
                    width: size,
                    height: size
                },
                style
            ]}
            onPress={onPress}>
            <Image
                style={{
                    width: size,
                    height: size,
                    tintColor: color
                }}
                resizeMode={"contain"}
                source={icon} />
        </TouchableOpacity>
    )
}

export default IconButton