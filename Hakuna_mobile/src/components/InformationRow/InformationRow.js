import { useTranslation } from "react-i18next";
import { Image, StyleSheet, Text, View } from "react-native"
import Color from "../../globals/Color"
import { iconFemaleDot, iconFlagVN, iconMaleDot, iconUndefineDot } from "../Icon"
import { Top } from "../Tags"

function InformationRow({ gender, rankStar, rankDiamod }) {

    const { t } = useTranslation();

    const _gender = gender == "male" ? t("Male") : (gender == "female" ? t("Female") : t("Undefined"));
    const iconGender = gender == "male" ? iconMaleDot : (gender == "female" ? iconFemaleDot : iconUndefineDot);

    return (
        <View style={styles.wrapper}>
            <Top
                type={"star"}
                rank={rankStar} />
            <Top
                type={"diamond"}
                rank={rankDiamod} />
            <View style={styles.devide}></View>
            <Image
                style={styles.iconCountry}
                source={iconFlagVN}
                resizeMode={"contain"} />
            <Text style={styles.country}>VN</Text>
            <View style={styles.dot}></View>
            <Image
                style={styles.iconGender}
                source={iconGender}
                resizeMode={"contain"} />
            <Text style={styles.gender}>{_gender}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    devide: {
        width: 1,
        height: 10,
        backgroundColor: Color.gray,
        marginLeft: 6,
        marginRight: 10
    },
    iconCountry: {
        width: 14,
        height: 14
    },
    country: {
        marginLeft: 3
    },
    dot: {
        width: 4,
        height: 4,
        borderRadius: 10,
        backgroundColor: Color.gray,
        marginHorizontal: 8
    },
    iconGender: {
        width: 14,
        height: 14
    },
    gender: {
        marginLeft: 2
    }
})

export default InformationRow