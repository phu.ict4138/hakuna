import iconHome from "../../assets/icon/home.png";
import iconSearch from "../../assets/icon/search.png";
import iconMessage from "../../assets/icon/message.png";
import iconPerson from "../../assets/icon/person.png";
import iconAdd from "../../assets/icon/add.png";
import iconStore from "../../assets/icon/store.png";
import iconCancel from "../../assets/icon/cancel.png";
import iconDiamond from "../../assets/icon/diamond.png";
import iconSetting from "../../assets/icon/setting.png";
import iconMale from "../../assets/icon/male.png";
import iconFemale from "../../assets/icon/female.png";
import iconGrayStar from "../../assets/icon/gray_star_2.png";
import iconGrayDiamond from "../../assets/icon/gray_diamond.png";
import iconMaleDot from "../../assets/icon/male_dot.png";
import iconFemaleDot from "../../assets/icon/female_dot.png";
import iconUndefineDot from "../../assets/icon/undefine_dot.png";
import iconUndefine from "../../assets/icon/undefine.png";
import iconStar from "../../assets/icon/star.png";
import iconFlagVN from "../../assets/icon/flag_vn.png";
import iconPlus from "../../assets/icon/plus.png";
import iconSearchSmall from "../../assets/icon/search_small.png";
import iconShop from "../../assets/icon/shop.png";
import iconBell from "../../assets/icon/bell.png";

import iconCrownSmallBronze from "../../assets/icon/crown_small_bronze.png";

import iconGroupHeart from "../../assets/icon/group_heart.png";


export {
    iconHome,
    iconSearch,
    iconMessage,
    iconPerson,
    iconAdd,
    iconStore,
    iconCancel,
    iconDiamond,
    iconSetting,
    iconFemale,
    iconMale,
    iconGrayDiamond,
    iconGrayStar,
    iconFemaleDot,
    iconMaleDot,
    iconUndefine,
    iconUndefineDot,
    iconStar,
    iconFlagVN,
    iconCrownSmallBronze,
    iconGroupHeart,
    iconPlus,
    iconSearchSmall,
    iconShop,
    iconBell
}