import { View } from "react-native";
import Color from "../../globals/Color";

function Devide({ height = 1, p = 0, mt = 0, mb = 0, color = Color.devide }) {

    return (
        <View style={{
            height: height,
            backgroundColor: color,
            marginHorizontal: p,
            marginTop: mt,
            marginBottom: mb
        }}></View>
    );
}

export default Devide