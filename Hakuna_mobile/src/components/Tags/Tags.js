import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useTranslation } from "react-i18next";
import { Image, StyleSheet, Text, View } from "react-native";
import Color from "../../globals/Color";
import { iconDiamond, iconGrayDiamond, iconGrayStar, iconStar } from "../Icon";

function PayTag() {

    const { t } = useTranslation();

    return (
        <View style={pay.wrapper}>
            <Text style={pay.text}>{t("Purchase")}</Text>
        </View>
    );
}

const pay = StyleSheet.create({
    wrapper: {
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 8,
        paddingVertical: 3,
        backgroundColor: Color.darkBlue,
        borderRadius: 99,
    },
    text: {
        color: Color.white,
        fontSize: 11
    }
});

function SubscribeTag() {

    const { t } = useTranslation();

    return (
        <View style={subscribe.wrapper}>
            <Text style={subscribe.text}>{t("Subscribe")}</Text>
            <FontAwesomeIcon
                icon={faAngleRight}
                style={subscribe.icon}
                size={13} />
        </View>
    );
}

const subscribe = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 10,
        paddingVertical: 4,
        backgroundColor: Color.darkOrange,
        borderRadius: 99,
    },
    text: {
        color: Color.white,
        fontWeight: "700",
        fontSize: 13
    },
    icon: {
        color: Color.white,
        marginLeft: 2
    }
});

function Top({ type = "star", rank = 99 }) {

    const icon = rank <= 50 ? (type == "star" ? iconStar : iconDiamond) : (type == "star" ? iconGrayStar : iconGrayDiamond);
    const backgroundColor = rank <= 50 ? (type == "star" ? Color.darkOrange : Color.softBlue) : Color.white;
    const borderWidth = rank > 50 ? 1 : undefined;
    const borderStyle = rank > 50 ? "dotted" : undefined;
    const text = rank > 50 ? `TOP - ` : `TOP ${rank}`;
    const colorText = rank > 50 ? Color.gray : Color.white;
    const borderColor = rank > 50 ? Color.gray : undefined;

    return (
        <View style={[
            top.wrapper,
            {
                backgroundColor: backgroundColor,
                borderWidth: borderWidth,
                borderStyle: borderStyle,
                borderColor: borderColor
            }
        ]}>
            <Image
                style={top.icon}
                source={icon}
                resizeMode={"contain"} />
            <Text style={[
                top.text,
                { color: colorText }
            ]}>{text}</Text>
        </View>
    )
}

const top = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        paddingLeft: 2,
        paddingRight: 6,
        height: 17,
        borderRadius: 99,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 4
    },
    icon: {
        width: 12,
        height: 12
    },
    text: {
        color: Color.white,
        fontSize: 8,
        fontWeight: "600",
        marginLeft: 4
    }
});

export { PayTag, SubscribeTag, Top }