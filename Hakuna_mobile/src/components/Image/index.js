import diamondChest from "../../assets/image/diamond_chest.png";
import voi from "../../assets/image/voi.png";
import avatar_1 from "../../assets/image/1.jpg";
import avatar_2 from "../../assets/image/2.jpg";
import avatar_3 from "../../assets/image/3.jpg";
import avatar_4 from "../../assets/image/4.jpg";
import avatar_5 from "../../assets/image/5.jpg";

const Image = {
    diamondChest,
    avatar_1,
    avatar_2,
    avatar_3,
    avatar_4,
    avatar_5,
    voi
}

export {
    diamondChest,
    avatar_1,
    avatar_2,
    avatar_3,
    avatar_4,
    avatar_5,
    voi
}

export default Image