import { useState } from "react";
import {
    Image,
    Pressable,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";

import Color from "../../globals/Color"
import Utils from "../../Utils/Utils";

function CrownAvatarMain({
    scale = 1,
    avatarLink,
    crownLink,
    level,
    onPressAvatar,
    onPressLevel
}) {

    const [focus, setFocus] = useState(false);

    const tier = Utils.getTierByLevel(level);
    const color = Utils.getColorByLevel(level);

    return (
        <View style={{
            ...styles.wrapper,
            transform: [{ scale: scale }]
        }}>
            <Image
                style={styles.avatar}
                source={{ uri: avatarLink }} />
            {focus &&
                <View style={styles.overlay}></View>
            }
            <Pressable
                onPressIn={() => setFocus(true)}
                onPressOut={() => setFocus(false)}
                onPress={onPressAvatar}
                style={styles.crown}>
                <Image
                    style={styles.crown}
                    source={{ uri: crownLink }} />
            </Pressable>
            <View style={styles.bottom}>
                <TouchableOpacity style={{
                    ...styles.row,
                    backgroundColor: color
                }}
                    activeOpacity={onPressLevel ? 0.8 : 1}
                    onPress={onPressLevel}>
                    <Text style={styles.tie}>{tier}</Text>
                    <Text style={styles.level}>{`Lv. ${level}`}</Text>
                </TouchableOpacity>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    wrapper: {
        width: 160,
        height: 160,
        alignItems: "center",
        justifyContent: "center"
    },
    avatar: {
        width: 90,
        height: 90,
        borderRadius: 999
    },
    crown: {
        position: "absolute",
        width: 160,
        height: 160,
        left: 0,
        top: 0,
        zIndex: 1
    },
    bottom: {
        width: 200,
        height: 26,
        position: "absolute",
        bottom: 16,
        left: -20,
        zIndex: 2,
        alignItems: "center",
        justifyContent: "center"
    },
    row: {
        flexDirection: "row",
        backgroundColor: Color.primary,
        height: 26,
        paddingHorizontal: 12,
        alignItems: "center",
        borderRadius: 2,
        shadowColor: Color.black,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowRadius: 6,
        elevation: 2,
    },
    tie: {
        color: Color.white,
        fontSize: 11
    },
    level: {
        color: Color.white,
        fontSize: 11,
        marginLeft: 12
    },
    overlay: {
        backgroundColor: "rgba(0,0,0, 0.4)",
        width: 90,
        height: 90,
        borderRadius: 999,
        position: "absolute",
    }
})

export default CrownAvatarMain