import { useTranslation } from "react-i18next";
import { FlatList, StyleSheet, Text, View } from "react-native";

import FollowerRow from "../../components/FollowerRow";
import Header from "../../components/Header";
import Color from "../../globals/Color";

const data = [
    {
        id: 0,
        name: "Voi",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 1,
        name: "Khỉ",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 2,
        name: "Mèo",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
]

function Follower() {

    const { t } = useTranslation();

    return (
        <View style={styles.container}>
            <Header
                title={t("Follower")}
                goBack={true} />
            <FlatList
                data={data}
                renderItem={({ item }) => (
                    <FollowerRow
                        key={item.id}
                        {...item} />
                )} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color._ffffff,

    }
});

export default Follower