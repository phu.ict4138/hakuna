import { StyleSheet, Text, View } from "react-native";

function Subscribe() {

    return (
        <View style={styles.container}>
            <Text>Subscribe</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

export default Subscribe