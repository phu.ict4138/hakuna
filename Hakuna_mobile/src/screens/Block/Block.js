import { Button, StyleSheet, Text, View } from "react-native";
import { useTranslation } from "../../hooks";
import Utils from "../../Utils";

function Block() {

    const { i18n } = useTranslation();

    return (
        <View style={styles.container}>
            <Text>Block</Text>
            <Button
                title="Ok"
                onPress={() => {
                    i18n.changeLanguage("en");
                }} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    }
});

export default Block