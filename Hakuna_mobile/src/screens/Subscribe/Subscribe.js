import { StyleSheet, Text, View } from "react-native";

import Header from "../../components/Header";
import Color from "../../globals/Color";

function Subscribe() {

    return (
        <View style={styles.container}>
            <Header
                title={"Đăng Ký của tôi"}
                goBack={true} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    }
});

export default Subscribe