import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { StyleSheet, Text, View } from "react-native";
import Color from "../../../../globals/Color";

function Tags({ title, number, icon }) {

    return (
        <View style={styles.wrapper}>
            {number &&
                <Text style={styles.number}>{number}</Text>
            }
            {title &&
                <Text style={styles.title}>{title}</Text>
            }
            {icon &&
                <FontAwesomeIcon
                    style={styles.icon}
                    icon={icon}
                    size={14} />}
        </View>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        height: 26,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(60, 60, 60, 0.3)",
        borderRadius: 99,
        marginRight: 8,
        flexDirection: "row",
        minWidth: 26
    },
    number: {
        color: Color.lightYellow,
        fontSize: 12,
        paddingLeft: 8
    },
    title: {
        marginLeft: 4,
        color: Color.white,
        fontSize: 12,
        paddingRight: 8
    },
    icon: {
        color: Color.white
    }
});

export default Tags