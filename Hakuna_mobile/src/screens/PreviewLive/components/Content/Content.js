import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useEffect, useRef, useState } from "react";
import { Button, Dimensions, Image, ImageBackground, StyleSheet, View } from "react-native";
import { Camera, useCameraDevices } from "react-native-vision-camera";
import { voi } from "../../../../components/Image";
import Color from "../../../../globals/Color";

const { width } = Dimensions.get("window");

const color = [
    "#1c1c1c",
    "#292929",
    "#353535"
]

function RadioLive({ numberGuest }) {

    const Layout = () => {
        if (numberGuest == 0) {
            return (
                <View style={radio.wrapper}>
                    <ImageBackground
                        style={radio.blur}
                        source={voi}
                        resizeMode={"cover"}
                        blurRadius={10}>
                        <View style={radio.overlay1}></View>
                    </ImageBackground>
                    <Image
                        style={radio.avatar0}
                        source={voi}
                        resizeMode={"cover"} />
                    <ImageBackground
                        style={radio.blur}
                        source={voi}
                        resizeMode={"cover"}
                        blurRadius={10}>
                        <View style={radio.overlay1}></View>
                    </ImageBackground>
                    <View style={radio.overlay0}></View>
                </View>
            );
        }
        else {
            const array = [];
            for (let i = 0; i < numberGuest; i++) {
                array.push(i);
            }
            return (
                <View style={radio.wrapper}>
                    <View style={radio.top}>
                        <Image
                            style={radio.top}
                            resizeMode={"cover"}
                            source={voi} />
                        <View style={radio.overlay0}></View>
                    </View>

                    <View style={radio.bottom}>
                        {array.map((item, index) => (
                            <View
                                key={index}
                                {...item}
                                style={{
                                    ...radio.item,
                                    backgroundColor: color[index]
                                }}>
                                <FontAwesomeIcon
                                    style={radio.icon}
                                    icon={faUser}
                                    size={14}
                                />
                            </View>
                        ))}
                    </View>
                </View>
            )
        }
    }

    return (
        <View style={radio.container}>
            <Layout />
        </View>
    );
}

const radio = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "red",
        borderRadius: 20,
        overflow: "hidden"
    },
    wrapper: {
        flex: 1,
        alignItems: "center",
    },
    avatar0: {
        width: width,
        height: width,
        zIndex: 2
    },
    blur: {
        flex: 1,
        backgroundColor: "blue",
        width: "100%",
    },
    overlay0: {
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0,
        backgroundColor: "rgba(0, 0, 0, 0.3)",
        zIndex: 2,
    },
    overlay1: {
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0,
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        zIndex: 1,
    },
    top: {
        flex: 1,
        width: "100%"
    },
    bottom: {
        flex: 1,
        width: "100%",
        flexDirection: "row",
        backgroundColor: "white"
    },
    item: {
        flex: 1,
        height: "100%",
        width: 100
    },
    icon: {
        marginTop: 20,
        marginLeft: 12,
        color: Color.pullGray
    }
});

function VideoLive({ numberGuest }) {

    const devices = useCameraDevices()
    const device = devices.front
    const ref = useRef();
    const [active, setActive] = useState(true);

    useEffect(() => {
        console.log(ref.current)
    }, [])

    if (device == null) return <View style={video.container}></View>
    return (
        <View style={video.container}>
            <Camera
                ref={ref}
                style={StyleSheet.absoluteFill}
                device={device}
                isActive={active} />
            <View style={{ width: "100%", position: "absolute", bottom: 10, right: 0 }}>

                <Button
                    title="hi"
                    onPress={() => {
                        console.log(Math.random());
                        setActive(!active)
                    }} />
            </View>
        </View>
    );
}

const video = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "blue",
        borderRadius: 20
    }
});

export { RadioLive, VideoLive }