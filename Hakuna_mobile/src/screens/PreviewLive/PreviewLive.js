import { useDispatch, useSelector } from "react-redux";
import { faAngleRight, faLock, faLockOpen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";

import { iconCancel } from "../../components/Icon";
import IconButton from "../../components/IconButton";
import { voi } from "../../components/Image";
import Color from "../../globals/Color";
import Tag from "./components/Tag";
import { useNavigation } from "@react-navigation/native";
import { setSetting, setTypeLive } from "./previewLiveSlice";
import { RadioLive, VideoLive } from "./components/Content/Content";

function PreviewLive() {

    const { setting } = useSelector(state => state.previewLive);

    const {
        title,
        wellcome,
        numberGuest,
        keyword,
        privateLive,
        listMember,
        showRankingLive,
        showRankingView,
        typeLive
    } = setting;
    const dispatch = useDispatch();

    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                <View style={styles.content}>
                    {typeLive == "video" ?
                        <VideoLive
                            numberGuest={numberGuest} />
                        :
                        <RadioLive
                            numberGuest={numberGuest} />
                    }
                </View>
                <IconButton
                    size={22}
                    style={styles.iconBack}
                    color={Color.white}
                    icon={iconCancel}
                    onPress={() => navigation.goBack()} />
                <View style={styles.setting}>
                    <TouchableOpacity
                        style={styles.wrapper}
                        activeOpacity={0.8}
                        onPress={() => navigation.navigate("SettingLive")}>
                        <Image
                            style={styles.avatar}
                            resizeMode={"contain"}
                            source={voi} />
                        <View style={styles.center}>
                            <View style={styles.top}>
                                <Tag
                                    title={keyword ? keyword : "Chọn chủ đề"} />
                                <Tag
                                    number={numberGuest.toString()}
                                    title={"khách"} />
                                <Tag
                                    icon={privateLive ? faLock : faLockOpen} />
                            </View>
                            <Text style={styles.title}>{title}</Text>
                            <Text style={styles.wellcome}>{wellcome ? wellcome : "Chào mừng người xem của bạn"}</Text>
                        </View>
                        <FontAwesomeIcon
                            style={styles.iconArrow}
                            icon={faAngleRight}
                            size={20} />

                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.button}>
                <TouchableOpacity
                    style={styles.btn}
                    activeOpacity={0.8}
                    onPress={() => 1}>
                    <Text style={styles.btnTitle}>Bắt đầu phát</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.bottom}>
                <TouchableOpacity
                    style={styles.type}
                    activeOpacity={typeLive == "video" ? 1 : 0.8}
                    onPress={() => {
                        if (typeLive != "video") {
                            dispatch(setSetting({ ...setting, typeLive: "video" }));
                        }
                    }}>
                    <Text style={{
                        ...styles.text,
                        color: typeLive == "video" ? Color.white : Color.darkGray,
                        fontWeight: typeLive == "video" ? "600" : undefined,
                    }}>Video</Text>
                    <View style={{
                        ...styles.line,
                        backgroundColor: typeLive == "video" ? Color.white : undefined,
                    }}></View>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.type}
                    activeOpacity={typeLive == "radio" ? 1 : 0.8}
                    onPress={() => {
                        if (typeLive != "radio") {
                            dispatch(setSetting({ ...setting, typeLive: "radio" }));
                        }
                    }}>
                    <Text style={{
                        ...styles.text,
                        color: typeLive == "radio" ? Color.white : Color.darkGray,
                        fontWeight: typeLive == "radio" ? "600" : undefined,
                    }}>Radio</Text>
                    <View style={{
                        ...styles.line,
                        backgroundColor: typeLive == "radio" ? Color.white : undefined,
                    }}></View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.black
    },
    iconBack: {
        position: "absolute",
        right: 20,
        top: 20
    },
    setting: {
        position: "absolute",
        top: 70,
        left: 0,
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 16,
        flexDirection: "row"
    },
    wrapper: {
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        flex: 1,
        height: 100,
        borderRadius: 12,
        flexDirection: "row",
        alignItems: "center"
    },
    avatar: {
        height: 50,
        width: 50,
        borderRadius: 8,
        marginLeft: 16
    },
    center: {
        flex: 1,
        paddingLeft: 12
    },
    top: {
        flexDirection: "row",
    },
    title: {
        fontSize: 16,
        color: Color.white,
        fontWeight: "600",
        marginTop: 4
    },
    wellcome: {
        color: Color.white,
        fontSize: 12,
        marginTop: 4
    },
    iconArrow: {
        marginRight: 16,
        color: Color.white
    },
    content: {
        position: "absolute",
        width: "100%",
        height: "100%"
    },
    button: {
        position: "absolute",
        bottom: 60,
        width: "100%",
        height: 50,
        alignItems: "center"
    },
    btn: {
        width: "50%",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Color.white,
        borderRadius: 99
    },
    btnTitle: {
        color: Color.black,
        fontWeight: "700"
    },
    bottom: {
        height: 40,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    type: {
        marginHorizontal: 10,
        width: 50,
        alignItems: "center"
    },
    text: {
        color: Color.darkGray,
        marginBottom: 10
    },
    line: {
        width: 50,
        height: 5,
        borderRadius: 3,
        backgroundColor: Color.white
    }
});

export default PreviewLive