import { createSlice } from "@reduxjs/toolkit";

const previewLiveSlice = createSlice({
    name: "previewLiveSlice",
    initialState: {
        setting: {
            title: `Voi 🐘's live`,
            wellcome: "",
            numberGuest: 0,
            keyword: "",
            privateLive: false,
            listMember: [],
            showRankingLive: false,
            showRankingView: false,
            typeLive: "radio"
        }
    },
    reducers: {
        setSetting(state, { payload }) {
            state.setting = payload
        },
        setTitle(state, { payload }) {
            state.title = payload
        },
        setWellcome(state, { payload }) {
            state.wellcome = payload
        },
        setNumberGuest(state, { payload }) {
            state.numberGuest = payload
        },
        setKeyword(state, { payload }) {
            state.keyword = payload
        },
        setPrivateLive(state, { payload }) {
            state.privateLive = payload
        },
        setListMember(state, { payload }) {
            state.listMember = payload
        },
        setShowRankingLive(state, { payload }) {
            state.showRankingLive = payload
        },
        setShowRankingView(state, { payload }) {
            state.showRankingView = payload
        },
        setTypeLive(state, { payload }) {
            state.typeLive = payload
        }
    }
});

const previewLiveReducer = previewLiveSlice.reducer;
const { actions } = previewLiveSlice;

const {
    setTitle,
    setWellcome,
    setNumberGuest,
    setKeyword,
    setPrivate,
    setListMember,
    setShowRankingLive,
    setShowRankingView,
    setTypeLive,
    setTopic,
    setSetting
} = actions;

export default previewLiveReducer
export {
    setTitle,
    setWellcome,
    setNumberGuest,
    setKeyword,
    setPrivate,
    setListMember,
    setShowRankingLive,
    setShowRankingView,
    setTypeLive,
    setTopic,
    setSetting
}