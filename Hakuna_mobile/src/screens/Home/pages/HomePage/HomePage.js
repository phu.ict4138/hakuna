import { useNavigation } from "@react-navigation/native";
import { Button, FlatList, ScrollView, StyleSheet, Text, View } from "react-native"
import { iconBell, iconShop } from "../../../../components/Icon";
import IconButton from "../../../../components/IconButton";
import Color from "../../../../globals/Color";
import Const from "../../../../globals/Const";
import FontSize from "../../../../globals/FontSize";
import BannerLiveSuggest from "./components/BannerLiveSuggest";

function HomePage() {

    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.hakuna}>HAKUNA</Text>
                <View style={styles.action}>
                    <IconButton
                        icon={iconShop}
                        size={30} />
                    <IconButton
                        style={styles.iconBell}
                        icon={iconBell}
                        size={30} />
                </View>
            </View>
            <Button
                title="test components"
                onPress={() => navigation.navigate("Test")} />
            <FlatList
                ListHeaderComponent={<BannerLiveSuggest />} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color._ffffff
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: Const.const_12,
        alignItems: "center",
        height: Const.const_48
    },
    hakuna: {
        fontSize: FontSize.const_24,
        fontWeight: "900"
    },
    action: {
        flexDirection: "row"
    },
    iconBell: {
        marginLeft: Const.const_8
    }
});

export default HomePage