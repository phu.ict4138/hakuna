import { Dimensions, FlatList, StyleSheet, View } from "react-native";

const { width } = Dimensions.get("window");

function BannerLiveSuggest() {

    return (
        <View style={styles.wrapper}>
            <FlatList
                nestedScrollEnabled={true} />
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        height: width * 0.7,
        backgroundColor: "red"
    }
});

export default BannerLiveSuggest  