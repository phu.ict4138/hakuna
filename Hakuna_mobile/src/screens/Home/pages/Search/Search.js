import { faChevronLeft, faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useState } from "react";
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";

import { iconSearchSmall } from "../../../../components/Icon";
import Color from "../../../../globals/Color";
import Recommend from "./components/Recommend";

function Search({ }) {

    const [searching, setSearching] = useState(false);
    const [searchValue, setSearchValue] = useState(false);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                {searching &&
                    <TouchableOpacity onPress={() => setSearching(false)}>
                        <FontAwesomeIcon
                            style={styles.iconBack}
                            icon={faChevronLeft}
                            size={18} />
                    </TouchableOpacity>}
                <View style={styles.border}>
                    <Image
                        style={styles.iconSearch}
                        source={iconSearchSmall}
                        resizeMode={"contain"} />
                    <TextInput
                        style={styles.input}
                        value={searchValue}
                        onChangeText={text => setSearchValue(text)}
                        placeholder={"Nhập tên hoặc ID Hakuna"}
                        onFocus={() => {
                            if (!searching) {
                                setSearching(true);
                            }
                        }}
                        onBlur={() => setSearching(false)} />
                    {searchValue.length > 0 &&
                        <TouchableOpacity onPress={() => setSearchValue("")}>
                            <FontAwesomeIcon
                                style={styles.iconClear}
                                icon={faCircleXmark}
                                size={14} />
                        </TouchableOpacity>
                    }
                </View>
            </View>
            <View style={styles.content}>
                {searching ? <View></View> : <Recommend />}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    header: {
        height: 60,
        marginHorizontal: 12,
        flexDirection: "row",
        alignItems: "center"
    },
    iconBack: {
        marginRight: 8
    },
    border: {
        flex: 1,
        backgroundColor: Color.lightGray,
        borderRadius: 5,
        height: 36,
        flexDirection: "row",
        alignItems: "center"
    },
    iconSearch: {
        width: 24,
        height: 24,
        marginLeft: 4, marginRight: 6
    },
    input: {
        flex: 1
    },
    iconClear: {
        color: Color.gray,
        marginRight: 8,
        marginLeft: 4
    },
    content: {
        flex: 1
    }
})

export default Search