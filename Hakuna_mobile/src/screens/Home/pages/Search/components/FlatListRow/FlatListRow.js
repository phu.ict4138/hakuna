import { FlatList, StyleSheet, Text, View } from "react-native";
import Const from "../../../../../../globals/Const";
import FontSize from "../../../../../../globals/FontSize";
import UserCard from "../UserCard";


const data = [
    {
        id: 0,
        name: "Voi",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 1,
        name: "Khỉ",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 2,
        name: "Mèo",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 3,
        name: "Voi",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 4,
        name: "Khỉ",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 5,
        name: "Mèo",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 6,
        name: "Voi",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 7,
        name: "Khỉ",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
    {
        id: 8,
        name: "Mèo",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg",
        crown: "https://d1tdolypiijgy2.cloudfront.net/images/crown/profile-decoration/compact/v3/Silver_v3.png"
    },
]

function FlatListRow({ title }) {

    return (
        <View style={styles.wrapper}>
            <Text style={styles.title}>{title}</Text>
            <FlatList
                data={data}
                horizontal={true}
                ListHeaderComponent={<View style={{ width: Const.const_12 }}></View>}
                ListFooterComponent={<View style={{ width: Const.const_6 }}></View>}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => (
                    <UserCard
                        key={item.id}
                        {...item} />
                )} />
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        marginTop: Const.const_32
    },
    title: {
        fontSize: FontSize.const_18,
        fontWeight: "600",
        marginLeft: Const.const_12,
        marginBottom: Const.const_8
    }
});

export default FlatListRow