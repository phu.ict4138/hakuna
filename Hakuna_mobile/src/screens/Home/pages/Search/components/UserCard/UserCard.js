import { useState } from "react";
import { Image, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Color from "../../../../../../globals/Color";
import Const from "../../../../../../globals/Const";

function UserCard({ name, avatar }) {

    const [focus, setFocus] = useState(false);

    return (
        <View style={{
            ...styles.wrapper
        }}>
            <Pressable
                onPressIn={() => setFocus(true)}
                onPressOut={() => setFocus(false)}>
                <Image
                    style={{
                        ...styles.avatar,
                        width: 150,
                        height: 150
                    }}
                    source={{ uri: avatar }}
                    resizeMode={"contain"} />
                <Text
                    style={styles.name}
                    numberOfLines={1}
                    ellipsizeMode={"tail"}>
                    {name}
                </Text>
                {focus && <View style={styles.overlay}></View>}
            </Pressable>
            <TouchableOpacity
                style={styles.button}
                activeOpacity={0.7}>
                <Text style={styles.text}>Theo dõi</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        marginRight: Const.const_8
    },
    avatar: {
        borderRadius: Const.const_8
    },
    name: {
        marginVertical: Const.const_8
    },
    overlay: {
        position: "absolute",
        top: Const.const_0,
        right: Const.const_0,
        width: 150,
        height: 150,
        backgroundColor: "rgba(0, 0, 0, 0.1)",
        borderRadius: Const.const_8
    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Color._feca00,
        height: Const.const_36,
        borderRadius: Const.const_8
    },
    text: {
        fontWeight: "700"
    }
});

export default UserCard