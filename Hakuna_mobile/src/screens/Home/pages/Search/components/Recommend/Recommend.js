import { ScrollView } from "react-native"
import Devide from "../../../../../../components/Devide"
import FlatListRow from "../FlatListRow"

function Recommend() {

    return (
        <ScrollView>
            <FlatListRow
                title={"Host Nổi Tiếng"} />
            <FlatListRow
                title={"Host Thình Hành"} />
            <FlatListRow
                title={"Những Host nữ nổi tiếng"} />
            <FlatListRow
                title={"Những người theo dõi hang đầu"} />
            <FlatListRow
                title={"Đề xuất của Hakuna"} />
            <Devide
                height={16} />
        </ScrollView>
    )
}

export default Recommend