import { Image, Pressable, StyleSheet, Text, View } from "react-native";

import Color from "../../../../../../globals/Color";


function MessageRow({ name, message, time, avatar, onPress }) {

    return (
        <Pressable
            style={({ pressed }) => ({
                ...styles.wrapper,
                backgroundColor: pressed ? Color.lightGray : Color.white
            })}
            onPress={onPress}>
            <Image
                style={styles.avatar}
                source={{ uri: avatar }}
                resizeMode={"contain"} />
            <View style={styles.content}>
                <Text
                    style={styles.name}
                    numberOfLines={1}>
                    {name}
                </Text>
                <Text style={styles.message}>{message}</Text>
            </View>
            <Text style={styles.time}>{time}</Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        paddingHorizontal: 14,
        height: 60,
        flexDirection: "row",
        alignItems: "center",
    },
    avatar: {
        width: 48,
        height: 48,
        borderRadius: 99,
        borderWidth: 1,
        borderColor: Color.gray
    },
    content: {
        flex: 1,
        marginLeft: 12
    },
    name: {

    },
    message: {
        color: Color.taupeGray
    },
    time: {
        color: Color.gray,
        fontSize: 12
    }
});

export default MessageRow