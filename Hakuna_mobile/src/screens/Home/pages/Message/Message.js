import { FlatList, Image, StyleSheet, TouchableOpacity, View } from "react-native"

import Header from "../../../../components/Header/Header";
import MessageRow from "./components/MessageRow";
import { iconPlus } from "../../../../components/Icon";
import Color from "../../../../globals/Color";

const data = [
    {
        id: 1,
        name: "Voi 😊😂🤣 💕💕💕💕",
        message: "ê",
        time: "2022-01-03",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg"
    },
    {
        id: 2,
        name: "Voi 😊😂🤣 sad",
        message: "ê",
        time: "2022-01-03",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg"
    },
    {
        id: 3,
        name: "Voi 😊😂🤣",
        message: "ê",
        time: "2022-01-03",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg"
    },
    {
        id: 4,
        name: "Voi 😊😂🤣",
        message: "ê sad ads d",
        time: "2022-01-03",
        avatar: "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg"
    },
]

function Message() {

    return (
        <View style={styles.container}>
            <Header
                title={"Tin nhắn trực tiếp"}
                rightComponent={
                    <TouchableOpacity>
                        <Image
                            style={styles.plus}
                            resizeMode={"contain"}
                            source={iconPlus} />
                    </TouchableOpacity>
                } />
            <FlatList
                data={data}
                renderItem={({ item }) => (
                    <MessageRow
                        key={item.id}
                        {...item} />
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    plus: {
        width: 26,
        height: 26,
        marginRight: 16
    }
})

export default Message