// import { useTranslation } from "react-i18next";
import { useNavigation } from "@react-navigation/native";
import {
    Image,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View
} from "react-native";

import ButtonRow from "./component/ButtonRow";
import Color from "../../../../globals/Color";
import { iconStore, iconSetting, iconStar } from "../../../../components/Icon"
import Devide from "../../../../components/Devide/Devide";
import { PayTag, SubscribeTag } from "../../../../components/Tags";
import IconButton from "../../../../components/IconButton";
import ButtonDiamond from "./component/ButtonDiamond";
import ButtonInvite from "./component/ButtonInvite/ButtonInvite";
import CrownAvatarMain from "../../../../components/CrownAvatarMain/CrownAvatarMain";
import InformationRow from "../../../../components/InformationRow/InformationRow";
import CrownTier from "./component/CrownTier/CrownTier";
import Group from "./component/Group";
import { useTranslation } from "../../../../hooks";

const ava = "https://profile.hakuna.live/public/2022-07-30/15b4a166-3062-45c1-9701-598eb9dd4487.jpg";
const cro = "https://static.hakuna.live/images/crown/profile-decoration/full/v2/RoseGold_v2.png";

function Person() {

    const { t } = useTranslation();
    const navigation = useNavigation();
    const status = "Đã bán acc";

    return (
        <View style={styles.container}>
            <StatusBar
                barStyle={"dark-content"} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.top}>
                    <ButtonDiamond
                        number={24} />
                    <IconButton
                        icon={iconSetting}
                        size={30} />
                </View>
                <View style={styles.avatar}>
                    <CrownAvatarMain
                        avatarLink={ava}
                        crownLink={cro}
                        level={39}
                        onPressAvatar={() => console.log(Math.random())}
                        onPressLevel={() => console.log(Math.random())}
                    />
                    <Text style={styles.name}>{"N i a r 🍃"}</Text>
                    <InformationRow
                        gender={"female"}
                        rankStar={10}
                        rankDiamod={20} />
                    {status &&
                        <Text style={styles.status}>{""}</Text>
                    }
                </View>
                <Devide
                    height={10} />
                <ButtonRow
                    title={t("MySubscribe")}
                    rightComponent={<SubscribeTag />}
                    onPress={() => navigation.navigate("Subscribe")} />
                <ButtonRow
                    title={t("MyCollection")} />
                <ButtonRow
                    title={t("Store")}
                    leftComponent={
                        <View>
                            <Image
                                style={styles.iconStore}
                                source={iconStore}
                                resizeMode={"contain"} />
                        </View>
                    } />
                <Devide
                    height={10} />
                <ButtonRow
                    title={t("MyLevel")}
                    rightComponent={
                        <CrownTier
                            level={11} />
                    } />
                <ButtonRow
                    title={t("Group")}
                    rightComponent={
                        <Group />
                    } />
                <ButtonRow
                    title={t("FanBoard")} />
                <Devide
                    height={10} />
                <ButtonRow
                    title={t("RankFollower")} />
                <ButtonRow
                    title={t("Follower")}
                    rightComponent={6431}
                    onPress={() => navigation.navigate("Follower")} />
                <ButtonRow
                    title={t("Following")}
                    rightComponent={31}
                    onPress={() => navigation.navigate("Following")} />
                <ButtonInvite
                    onPress={() => 1} />
                <Devide
                    height={10} />
                <ButtonRow
                    leftComponent={
                        <View style={styles.row}>
                            <Text style={styles.wallet}>{t("StarWallet")}</Text>
                            <PayTag />
                        </View>
                    }
                    rightComponent={
                        <View style={styles.row}>
                            <Image
                                style={styles.iconStar}
                                resizeMode={"contain"}
                                source={iconStar} />
                            <Text>{999}</Text>
                        </View>
                    }
                />
                <Devide
                    height={10} />
                <ButtonRow
                    title={t("Priority")} />
                <ButtonRow
                    title={t("BlockList")}
                    onPress={() => navigation.navigate("Block")} />
                <ButtonRow
                    title={t("HelpCenter")} />
                <ButtonRow
                    title={t("Juridical")} />
                <ButtonRow
                    title={t("Feedback")} />
                <Devide
                    height={10} />
                <ButtonRow
                    title={t("IDHakuna")}
                    rightComponent={"alkhshu81e3544"}
                    styled={true} />
                <ButtonRow
                    title={t("Version")}
                    rightComponent={"1.3.3"}
                    styled={true} />
                <Devide
                    height={10} />
                <ButtonRow
                    title={t("Logout")} />
                <Devide
                    height={40} />
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    iconStore: {
        width: 28,
        height: 28,
        marginRight: 8
    },
    row: {
        flexDirection: "row",
        alignItems: "center"
    },
    wallet: {
        fontSize: 16,
        marginRight: 4
    },
    top: {
        flexDirection: "row",
        paddingHorizontal: 16,
        height: 60,
        alignItems: "center",
        justifyContent: "space-between",
        marginTop: 12
    },
    avatar: {
        alignItems: "center",
        paddingBottom: 36
    },
    name: {
        fontSize: 22,
        marginBottom: 8
    },
    infor: {
        flexDirection: "row",
        alignItems: "center"
    },
    gender: {
        width: 24, height: 24
    },
    status: {
        color: Color.darkGray,
        marginTop: 16
    },
    iconStar: {
        width: 15,
        height: 15,
        marginRight: 4
    }
});

export default Person