
import _ from "lodash";
import { Image, Pressable, StyleSheet, Text, View } from "react-native";

import Color from "../../../../../../globals/Color";

function ButtonRow({ title, onPress, styled, rightComponent, leftComponent }) {

    return (
        <Pressable
            style={({ pressed }) =>
                [
                    styles.wrapper,
                    { backgroundColor: pressed ? Color.lightGray : undefined }
                ]
            }
            onPress={onPress}>
            {leftComponent}
            <Text style={styles.title}>{title}</Text>
            <View style={styles.flex}></View>
            {_.isObject(rightComponent) ? rightComponent :
                <Text
                    style={styled ? styles.right : {}}>
                    {rightComponent}
                </Text>
            }
        </Pressable>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        height: 60,
        alignItems: "center",
        paddingHorizontal: 16,
        borderBottomColor: Color.lightGray,
        borderBottomWidth: 1 / 5
    },
    iconLeft: {
        width: 28,
        height: 28,
        marginRight: 8
    },
    title: {
        fontSize: 16
    },
    flex: {
        flex: 1
    },
    right: {
        fontSize: 12,
        color: Color.darkGray
    }

})

export default ButtonRow