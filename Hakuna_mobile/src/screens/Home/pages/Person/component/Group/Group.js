import { Image, StyleSheet, Text, View } from "react-native"
import { iconGroupHeart } from "../../../../../../components/Icon"
import Color from "../../../../../../globals/Color";

function Group() {

    return (
        <View style={styles.wrapper}>
            <Image
                style={styles.iconGroup}
                resizeMode={"contain"}
                source={iconGroupHeart} />
            <Text>{"『MOON』"}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center"
    },
    iconGroup: {
        width: 24,
        height: 24,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: Color.gray,
    },
    groupName: {
        marginLeft: 4
    }
});

export default Group