import { Image, StyleSheet, Text, View } from "react-native"

import { iconCrownSmallBronze } from "../../../../../../components/Icon"
import Utils from "../../../../../../Utils/Utils";

function CrownTier({ level }) {

    const getCrownByLevel = (level) => {
        return iconCrownSmallBronze;
    }

    const crown = getCrownByLevel(level);
    const tier = Utils.getTierByLevel(level);
    const color = Utils.getColorByLevel(level);

    return (
        <View style={styles.wrapper}>
            <Image
                style={styles.icon}
                resizeMode={"contain"}
                source={crown} />
            <Text style={{
                ...styles.tier,
                color: color
            }}>{`${tier} / Lv. ${level}`}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center"
    },
    icon: {
        width: 24,
        height: 24
    },
    tier: {
        marginLeft: 4
    }
})

export default CrownTier