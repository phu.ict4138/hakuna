import { useTranslation } from "react-i18next";
import { Image, StyleSheet, Text, TouchableOpacity } from "react-native";

import Color from "../../../../../../globals/Color";
import { diamondChest } from "../../../../../../components/Image"

function ButtonInvite({ onPress }) {

    const { t } = useTranslation();

    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.wrapper}
            onPress={onPress}>
            <Text style={styles.text}>{t("InviteFriend")}</Text>
            <Image
                style={styles.image}
                resizeMode={"contain"}
                source={diamondChest} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        paddingLeft: 16,
        backgroundColor: Color.violet,
        alignItems: "center",
        flexDirection: "row",
        height: 60,
    },
    text: {
        color: Color.white,
        flex: 1,
    },
    image: {
        height: 60,
        width: 112,
    }
})

export default ButtonInvite