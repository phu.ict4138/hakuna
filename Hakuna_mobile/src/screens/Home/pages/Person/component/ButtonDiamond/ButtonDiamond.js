import { Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import { iconDiamond } from "../../../../../../components/Icon";
import Color from "../../../../../../globals/Color";

function ButtonDiamond({ number = 0 }) {

    return (
        <TouchableOpacity style={styles.wrapper}>
            <Image
                source={iconDiamond}
                style={styles.icon} />
            <Text style={styles.text}>{number}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        height: 24,
        paddingHorizontal: 10,
        backgroundColor: Color.bemGray,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        borderRadius: 2
    },
    icon: {
        width: 14,
        height: 14
    },
    text: {
        marginLeft: 4,
        fontSize: 12,
        fontWeight: "600"
    }
});

export default ButtonDiamond