import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useNavigation } from "@react-navigation/native";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { PERMISSIONS, request, check } from "react-native-permissions"

import HomePage from "./pages/HomePage";
import Message from "./pages/Message";
import Person from "./pages/Person";
import Search from "./pages/Search";
import Color from "../../globals/Color";
import {
    iconHome,
    iconSearch,
    iconMessage,
    iconPerson,
    iconAdd
} from "../../components/Icon";

const Tab = createBottomTabNavigator();

function Home() {

    const navigation = useNavigation();

    const requestPM = async () => {
        const isGranted = await request(PERMISSIONS.ANDROID.CAMERA);
        console.log(isGranted)
        // const isGranted = true
        // console.log(isGranted);
        if (isGranted) {
            navigation.navigate("PreviewLive");
        }
    }

    return (
        <Tab.Navigator
            screenOptions={{
                tabBarShowLabel: false,
                tabBarStyle: { height: 60 },
                tabBarHideOnKeyboard: true
            }}>
            <Tab.Screen
                name="HomePage"
                component={HomePage}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <Image
                                style={[styles.icon, { tintColor: focused ? Color.black : Color.gray }]}
                                source={iconHome}
                            />
                        </View>
                    )
                }}
            />
            <Tab.Screen
                name="Search"
                component={Search}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <Image
                                style={[styles.icon, { tintColor: focused ? Color.black : Color.gray }]}
                                source={iconSearch}
                            />
                        </View>
                    )
                }}
            />
            <Tab.Screen
                name="Fake"
                component={Search}
                options={{
                    headerShown: false,
                    tabBarButton: (props) => (
                        <ButtonAdd
                            // onClick={() => navigation.navigate("PreviewLive")}
                            onClick={() => {
                                requestPM();

                            }}
                            {...props} />
                    )
                }}
            />
            <Tab.Screen
                name="Message"
                component={Message}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <Image
                                style={[styles.icon, { tintColor: focused ? Color.black : Color.gray }]}
                                source={iconMessage}
                            />
                        </View>
                    )
                }}
            />
            <Tab.Screen
                name="Person"
                component={Person}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <Image
                                style={[styles.icon, { tintColor: focused ? Color.black : Color.gray }]}
                                source={iconPerson}
                            />
                        </View>
                    )
                }}
            />

        </Tab.Navigator>
    );
}

function ButtonAdd({ onClick }) {

    return (
        <TouchableOpacity
            style={styles.wrapper}
            onPress={onClick}>
            <Image
                style={styles.add}
                source={iconAdd} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    icon: {
        width: 32,
        height: 32
    },
    wrapper: {
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 16,
        marginRight: 16
    },
    add: {
        width: 40,
        height: 40
    }
})

export default Home