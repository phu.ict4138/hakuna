import { Button, Text, View } from "react-native"
import Utils from "../../Utils";

function Test() {

    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Button
                title="Toast"
                onPress={() => Utils.toast("hello")} />
            <Button
                title="Show loading"
                onPress={() => Utils.showLoading("hahaha")} />
            <Button
                title="Show dialog"
                onPress={() => Utils.showNotifyDialog()} />
        </View>
    );
}

export default Test