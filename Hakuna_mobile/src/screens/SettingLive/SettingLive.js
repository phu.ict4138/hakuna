import { useLayoutEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { Image, ScrollView, StatusBar, StyleSheet, Switch, Text, TouchableOpacity, View } from "react-native"
import { useDispatch, useSelector } from "react-redux";

import Color from "../../globals/Color";
import EditText from "./components/EditText/EditText";
import Devide from "../../components/Devide";
import Tag from "./components/Tag";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faAngleRight, faLock, faLockOpen } from "@fortawesome/free-solid-svg-icons";
import { avatar_1, avatar_2, avatar_3, avatar_4, avatar_5, voi } from "../../components/Image";
import { setSetting } from "../PreviewLive/previewLiveSlice";

const users = [
    { id: 0, image: voi },
    { id: 1, image: avatar_1 },
    { id: 2, image: avatar_2 },
    { id: 3, image: avatar_3 },
    { id: 4, image: avatar_4 },
    { id: 5, image: avatar_5 },
    { id: 6, image: avatar_4 },
    { id: 7, image: avatar_3 },
    { id: 8, image: avatar_2 },
    { id: 9, image: avatar_1 },
    { id: 10, image: voi },
    { id: 11, image: avatar_1 },
    { id: 12, image: avatar_2 },
    { id: 13, image: avatar_3 },
    { id: 14, image: avatar_4 },
    { id: 15, image: avatar_5 },
    { id: 16, image: avatar_4 },
    { id: 17, image: avatar_3 },
    { id: 18, image: avatar_2 },
    { id: 19, image: avatar_1 },
]

function SettingLive() {

    const { setting } = useSelector(state => state.previewLive);

    const {
        title,
        wellcome,
        numberGuest,
        keyword,
        privateLive,
        listMember,
        showRankingLive,
        showRankingView,
        typeLive
    } = setting;
    const dispatch = useDispatch();

    const [titleValue, setTitleValue] = useState(title);
    const [wellcomeValue, setWellcomeValue] = useState(wellcome);
    const [numberGuestValue, setNumberGuestValue] = useState(numberGuest);
    const [keywordValue, setKeywordValue] = useState(keyword);
    const [privateLiveValue, setPrivateLiveValue] = useState(privateLive);
    const [listMemberValue, setListMemberValue] = useState(listMember);
    const [showRankingLiveValue, setShowRankingLiveValue] = useState(showRankingLive);
    const [showRankingViewValue, setShowRankingViewValue] = useState(showRankingView);
    const [types, setTypes] = useState(() => {
        const type = [
            { id: 0, value: "", active: keywordValue == "" },
            { id: 1, value: "Sự kiện", active: keywordValue == "Sự kiện" },
            { id: 2, value: "Âm nhạc", active: keywordValue == "Âm nhạc" },
            { id: 3, value: "Trò chơi", active: keywordValue == "Trò chơi" },
            { id: 4, value: "Hài kịch", active: keywordValue == "Hài kịch" },
            { id: 5, value: "K-Pop", active: keywordValue == "K-Pop" },
            { id: 6, value: "Kể chuyện", active: keywordValue == "Kể chuyện" },
            { id: 7, value: "Hẹn hò", active: keywordValue == "Hẹn hò" },
            { id: 8, value: "Làm đẹp", active: keywordValue == "Làm đẹp" },
            { id: 9, value: "Thú nhận", active: keywordValue == "Thú nhận" },
            { id: 10, value: "Thể thao", active: keywordValue == "Thể thao" },
            { id: 11, value: "Trò chuyện & Thư giãn", active: keywordValue == "Trò chuyện & Thư giãn" },
            { id: 12, value: "Otaku", active: keywordValue == "Otaku" }
        ]
        return type;
    });

    console.log("re-render ", Math.random())
    const navigation = useNavigation();

    const save = () => {
        dispatch(setSetting({
            title: titleValue,
            wellcome: wellcomeValue,
            numberGuest: numberGuestValue,
            keyword: keywordValue,
            privateLive: privateLiveValue,
            listMember: listMemberValue,
            showRankingLive: showRankingLiveValue,
            showRankingView: showRankingViewValue,
            typeLive: typeLive
        }))
        navigation.goBack();
    }



    return (
        <View style={styles.container}>
            <StatusBar />
            <View style={styles.header}>
                <TouchableOpacity
                    style={styles.left}
                    onPress={() => navigation.goBack()}>
                    <Text style={styles.leftText}>{"Hủy bỏ"}</Text>
                </TouchableOpacity>
                <Text style={styles.titleText}>{"Cài đặt live"}</Text>
                <TouchableOpacity
                    style={styles.save}
                    onPress={save}>
                    <Text style={styles.saveText}>{"Đã xong"}</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.content}>
                <EditText
                    title={"Tiêu đề"}
                    length={titleValue.length}
                    value={titleValue}
                    onChangeText={text => setTitleValue(text)}
                    maxLength={20}
                    placeholder={"Vui lòng nhậ tiêu đề"}
                    showButtonClear={titleValue.length > 0}
                    onClear={() => setTitleValue("")} />
                <EditText
                    title={"Tin nhắn chào mừng (không bắt buộc)"}
                    length={wellcomeValue.length}
                    value={wellcomeValue}
                    onChangeText={text => setWellcomeValue(text)}
                    maxLength={30}
                    placeholder={"Chào người xem của bạn"}
                    showButtonClear={wellcomeValue.length > 0}
                    onClear={() => setWellcomeValue("")} />
                <Devide
                    color={Color.darkGray}
                    height={0.2}
                    mt={12}
                    mb={30} />
                <Text style={styles.text}>{"Số lượng khách mời"}</Text>
                <View style={{
                    ...styles.wrapper,
                    flexWrap: "nowrap"
                }}>
                    <Tag
                        title={"0 người"}
                        flex={true}
                        ml={0}
                        active={numberGuestValue == 0}
                        onPress={() => {
                            if (numberGuestValue != 0) {
                                setNumberGuestValue(0)
                            }
                        }}
                        border={5} />
                    <Tag
                        title={"1 người"}
                        flex={true}
                        active={numberGuestValue == 1}
                        onPress={() => {
                            if (numberGuestValue != 1) {
                                setNumberGuestValue(1)
                            }
                        }}
                        border={5} />
                    <Tag
                        title={"2 người"}
                        flex={true}
                        active={numberGuestValue == 2}
                        onPress={() => {
                            if (numberGuestValue != 2) {
                                setNumberGuestValue(2)
                            }
                        }}
                        border={5} />
                    <Tag
                        title={"3 người"}
                        flex={true}
                        mr={0}
                        active={numberGuestValue == 3}
                        onPress={() => {
                            if (numberGuestValue != 3) {
                                setNumberGuestValue(3)
                            }
                        }}
                        border={5} />
                </View>
                <Devide
                    mb={20}
                    mt={20}
                    height={0.2}
                    color={Color.darkGray} />
                <Text style={styles.text}>{"Chọn một từ khóa"}</Text>
                <Text style={styles.textSmall}>{"Vui lòng chọn một từ khóa"}</Text>
                <View style={styles.wrapper}>
                    {types.slice(1, 13).map(item => (
                        <Tag
                            key={item.id}
                            title={item.value}
                            active={item.active}
                            mr={item.id % 4 == 0 ? 0 : undefined}
                            onPress={() => {
                                if (item.value != keywordValue) {
                                    setKeywordValue(item.value);
                                    setTypes(types.map(_item => ({
                                        ..._item,
                                        active: _item.value == item.value
                                    })))
                                }
                            }} />
                    ))}
                </View>
                <Devide
                    mb={12}
                    mt={20}
                    height={0.4}
                    color={Color.darkGray} />
                <View style={styles.row}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <FontAwesomeIcon
                            style={styles.iconLock}
                            icon={privateLiveValue ? faLock : faLockOpen}
                            size={12} />
                        <Text style={styles.text}>{"Phòng riêng"}</Text>
                    </View>
                    <Switch
                        value={privateLiveValue}
                        onChange={() => setPrivateLiveValue(!privateLiveValue)}
                        thumbColor={privateLiveValue ? Color.primary : Color.gray}
                        trackColor={{
                            true: Color.lightBrown,
                            false: Color.darkGray
                        }}
                        collapsable={true} />
                </View>
                <View style={styles.row}>
                    <View style={{
                        flex: 1,
                        flexDirection: "row",
                        height: 30,
                        overflow: "hidden"
                    }}>
                        <View style={{ flex: 1, flexDirection: "column" }}>
                            {users.map((item, index) => (
                                <Image
                                    key={index}
                                    style={{
                                        ...styles.image,
                                        position: "absolute",
                                        top: 0,
                                        zIndex: 9999 - index,
                                        left: index * 24
                                    }}
                                    resizeMode={"contain"}
                                    source={item.image} />
                            ))}
                        </View>
                    </View>
                    <Text style={styles.number}>{`${users.length} người`}</Text>
                    <FontAwesomeIcon
                        icon={faAngleRight}
                        style={styles.number}
                        size={12} />
                </View>
                <Devide
                    mb={12}
                    mt={28}
                    height={0.4}
                    color={Color.darkGray} />
                <View style={styles.row}>
                    <View>
                        <Text style={styles.text}>{"Hiển thị Xếp hạng Live"}</Text>
                        <Text style={styles.textSmall}>{"Ít người sẽ được tiếp xúc người xem hơn Host thấy"}</Text>
                    </View>
                    <Switch
                        value={showRankingLiveValue}
                        onChange={() => setShowRankingLiveValue(!showRankingLiveValue)}
                        thumbColor={showRankingLiveValue ? Color.primary : Color.gray}
                        trackColor={{
                            true: Color.lightBrown,
                            false: Color.darkGray
                        }}
                    />
                </View>
                <View style={styles.row}>
                    <View>
                        <Text style={styles.text}>{"Hiển thị Bảng xếp hạng"}</Text>
                        <Text style={styles.textSmall}>{"Ít người sẽ được tiếp xúc người xem hơn Host thấy"}</Text>
                    </View>
                    <Switch
                        value={showRankingViewValue}
                        onChange={() => setShowRankingViewValue(!showRankingViewValue)}
                        thumbColor={showRankingViewValue ? Color.primary : Color.gray}
                        trackColor={{
                            true: Color.lightBrown,
                            false: Color.darkGray
                        }}
                    />
                </View>
                <View style={{ height: 20 }}></View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.black
    },
    header: {
        height: 60,
        borderBottomWidth: 0.2,
        borderBottomColor: Color.darkGray,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center"
    },
    left: {
        flex: 1,
        height: "100%",
        justifyContent: "center",
        paddingLeft: 14
    },
    title: {
        flex: 3,
        fontSize: 16,
        height: "100%",
        justifyContent: "center"
    },
    save: {
        flex: 1,
        fontWeight: "600",
        height: "100%",
        justifyContent: "center",
        alignItems: "flex-end",
        paddingRight: 14
    },
    leftText: {
        color: Color.white
    },
    titleText: {
        color: Color.white,
        fontSize: 16,
        flex: 3,
        textAlign: "center"
    },
    content: {
        paddingHorizontal: 12
    },
    saveText: {
        color: Color.primary,
        fontWeight: "700"
    },
    text: {
        color: Color.white,
        fontWeight: "600",
        fontSize: 16
    },
    textSmall: {
        color: Color.gray,
        fontSize: 12,
        marginBottom: 12
    },
    wrapper: {
        flexDirection: "row",
        flexWrap: "wrap"
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 4
    },
    iconLock: {
        color: Color.white,
        marginRight: 4
    },
    number: {
        color: Color.white,
        paddingLeft: 8
    },
    image: {
        width: 30,
        height: 30, borderRadius: 99,
        borderWidth: 1,
        borderColor: Color.white
    }
});

export default SettingLive