import { faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { useState } from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import Color from "../../../../globals/Color";

function EditText({ title, length, maxLength, showButtonClear, onClear, value, placeholder, onChangeText }) {

    const [focus, setFocus] = useState(false);

    return (
        <View style={styles.wrapper}>
            <Text style={styles.title}>{title}</Text>
            <View style={{
                ...styles.border,
                borderColor: focus ? Color.darkGray : Color.lightBlack
            }}>
                <TextInput
                    onFocus={() => setFocus(true)}
                    onBlur={() => setFocus(false)}
                    style={styles.input}
                    value={value}
                    onChangeText={onChangeText}
                    placeholder={placeholder}
                    placeholderTextColor={Color.darkGray}
                    maxLength={maxLength} />
                {showButtonClear &&
                    <TouchableOpacity onPress={onClear}>
                        <FontAwesomeIcon
                            style={styles.iconClear}
                            icon={faCircleXmark}
                        />
                    </TouchableOpacity>
                }
            </View>
            <View style={styles.bottom}>
                <Text style={styles.length}>{length}</Text>
                <Text style={styles.maxLength}>{`/${maxLength}`}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 12
    },
    title: {
        color: Color.pullGray,
        fontSize: 12
    },
    border: {
        marginTop: 8,
        height: 44,
        backgroundColor: Color.lightBlack,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: Color.lightBlack,
        flexDirection: "row",
        paddingLeft: 8,
        alignItems: "center"
    },
    input: {
        flex: 1,
        color: Color.white,
        marginRight: 4
    },
    iconClear: {
        color: Color.gray,
        marginRight: 4
    },
    bottom: {
        marginTop: 4,
        flexDirection: "row",
        justifyContent: "flex-end"
    },
    length: {
        color: Color.white,
        fontSize: 12
    },
    maxLength: {
        color: Color.pullGray,
        fontSize: 12
    }
});

export default EditText