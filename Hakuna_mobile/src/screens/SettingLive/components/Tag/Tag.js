import { Dimensions, StyleSheet, Text, TouchableOpacity } from "react-native";

import Color from "../../../../globals/Color";

const { width } = Dimensions.get("window");

function Tag({
    title,
    border = 999,
    flex,
    ml = 8,
    mr = 10,
    active = false,
    onPress
}) {

    return (
        <TouchableOpacity style={{
            ...styles.wrapper,
            flex: flex ? 1 : 0,
            borderRadius: border,
            marginRight: mr,
            paddingHorizontal: flex ? 8 : 16,
            minWidth: flex ? undefined : 65,
            backgroundColor: active ? Color.white : Color.lightBlack
        }}
            onPress={onPress}
            activeOpacity={active ? 1 : 0.8}>
            <Text style={{
                ...styles.title,
                fontWeight: active ? "700" : "400",
                color: active ? Color.black : Color.white
            }}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        height: 40,
        justifyContent: "center",
        paddingHorizontal: 16,
        backgroundColor: Color.lightBlack,
        marginTop: 8,
        alignItems: "center",
    },
    title: {
        color: Color.white
    }
});

export default Tag