import Toast, { toastRef, options } from "./component/Toast";
import Loading, { loadingRef } from "./component/Loading";
import Color from "../globals/Color";
import { useTranslation } from "../hooks";
import NotifyDialog, { notifyDialogRef } from "./component/NotifyDialog";

const Utils = {
    toast(message, duration = options.time.LONG) {
        toastRef.current.show(message, duration);
    },
    hideToast() {
        toastRef.current.hide();
    },
    showLoading(message = "") {
        loadingRef.current.show(message);
    },
    hideLoading() {
        loadingRef.current.hide();
    },
    showNotifyDialog() {
        notifyDialogRef.current.show("hêl1", "a");
    }
    ,
    getColorByLevel(level) {
        if (level < 10) {
            return Color.iron;
        }
        if (level < 20) {
            return Color.bronze;
        }
        if (level < 30) {
            return Color.silver;
        }
        if (level < 40) {
            return Color.gold;
        }
        if (level < 50) {
            return Color.platinum;
        }
        if (level < 60) {
            return Color.diamond;
        }
        return Color.hero;
    },
    getTierByLevel(level) {
        const { t } = useTranslation();
        if (level < 3) {
            return t("Stone");
        }
        if (level < 10) {
            return t("Iron");
        }
        if (level < 15) {
            return t("Bronze");
        }
        if (level < 20) {
            return t("LuxuryBronze");
        }
        if (level < 25) {
            return t("Silver");
        }
        if (level < 30) {
            return t("BlackSilver");
        }
        if (level < 35) {
            return t("Gold");
        }
        if (level < 40) {
            return t("PinkGold");
        }
        if (level < 45) {
            return t("Platinum");
        }
        if (level < 50) {
            return t("Sapphire");
        }
        if (level < 55) {
            return t("Diamond");
        }
        if (level < 60) {
            return t("RedDiamond");
        }
        return t("Hero");
    }
}

function Util() {

    return (
        <>
            <Loading />
            <Toast />
            <NotifyDialog />
        </>
    );
}

export default Utils
export { Util }