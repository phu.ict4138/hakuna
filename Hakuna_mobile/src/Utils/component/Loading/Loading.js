import { createRef, useEffect, useState } from "react";
import {
    ActivityIndicator,
    Modal,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    View
} from "react-native";
import Color from "../../../globals/Color";

const loadingRef = createRef();

function Loading() {

    const [show, setShow] = useState(false);
    const [message, setMessage] = useState(false);

    useEffect(() => {
        loadingRef.current = {
            show(messages = "") {
                if (messages != message) {
                    setMessage(messages);
                }
                setShow(true);
            },
            hide() {
                setShow(false);
            }
        }
    }, []);

    return (
        <Modal
            visible={show}
            transparent={true}
            statusBarTranslucent={true}
        >
            <TouchableWithoutFeedback
                onPress={() => setShow(false)}>
                <View style={styles.overlay}>
                    <View style={{
                        ...styles.content,
                    }}>
                        <ActivityIndicator
                            size={"large"}
                            style={styles.loading}
                            color={Color.primary} />
                        {message &&
                            <Text style={styles.message}>{message}</Text>
                        }
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
}

const styles = StyleSheet.create({
    overlay: {
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.3)",
        alignItems: "center",
        justifyContent: "center",
    },
    content: {
        backgroundColor: Color.white,
        minWidth: 100,
        minHeight: 100,
        maxWidth: "80%",
        borderRadius: 10,
        padding: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    loading: {
        transform: [{ scale: 1.5 }]
    },
    message: {
        marginTop: 20
    }
})

export default Loading
export { loadingRef }