import { createRef, useEffect, useState } from "react"
import { Pressable, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Color from "../../../globals/Color";
import Const from "../../../globals/Const";
import Devide from "../../../components/Devide"

const notifyDialogRef = createRef();

function NotifyDialog() {
    const [show, setShow] = useState(false);
    const [message, setMessage] = useState("");
    const [title, setTitle] = useState("");

    useEffect(() => {
        notifyDialogRef.current = {
            show(message, title) {
                setShow(true);
            }
        }
    }, [])

    if (!show) {
        return <></>
    }
    return (
        <Pressable
            style={styles.overlay}
            onPress={() => setShow(false)}>
            <StatusBar backgroundColor={Color.overlay_3} />
            <View style={styles.content}>
                <Text style={styles.title}>Hello</Text>
                <Devide
                    height={0.4}
                    p={8} />
                <Text style={styles.message}>Bạn nhận được một cái nịt. Bạn có muốn lấy luôn không ?</Text>
                <Devide
                    height={0.4}
                    p={8} />
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.done}>{"Xác nhận"}</Text>
                </TouchableOpacity>
            </View>
        </Pressable>
    );
}
const styles = StyleSheet.create({
    overlay: {
        position: "absolute",
        bottom: Const.const_0,
        top: Const.const_0,
        left: Const.const_0,
        right: Const.const_0,
        backgroundColor: Color.overlay_3,
        alignItems: "center",
        justifyContent: "center"
    },
    content: {
        width: "80%",
        backgroundColor: Color._ffffff,
        borderRadius: Const.const_8,
        overflow: "hidden"
    },
    title: {
        textAlign: "center",
        marginTop: Const.const_4,
        marginBottom: Const.const_8,
        fontWeight: "700",
    },
    message: {
        paddingHorizontal: Const.const_12,
        marginTop: Const.const_4,
        minHeight: Const.const_60,
        textAlign: "center"
    },
    button: {
        height: Const.const_36,
        alignItems: "center",
        justifyContent: "center",
    },
    done: {
        fontWeight: "700",
        color: Color._feca00
    }
});

export default NotifyDialog
export {
    notifyDialogRef
}