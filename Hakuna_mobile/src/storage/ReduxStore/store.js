import { configureStore, applyMiddleware, compose } from "@reduxjs/toolkit";

import previewLiveReducer from "../../screens/PreviewLive/previewLiveSlice";

const reducer = {
    previewLive: previewLiveReducer
}

const logger = (_store) => (next) => (action) => {
    return next(action);
}

const store = configureStore({
    reducer: reducer,
    devTools: true,
    enhancers: [compose(applyMiddleware(logger))],
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    })
});

export default store
