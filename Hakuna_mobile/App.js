import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import store from "./src/storage/ReduxStore/store";
import i18next from "./src/languages/Language";
import { Hooks } from "./src/hooks";
import Toast from "./src/Utils/component/Toast";
import Loading from "./src/Utils/component/Loading";
import Home from "./src/screens/Home";
import Test from "./src/screens/Test";
import Subscribe from "./src/screens/Subscribe";
import PreviewLive from "./src/screens/PreviewLive";
import SettingLive from "./src/screens/SettingLive";
import Block from "./src/screens/Block";
import Follower from "./src/screens/Follower";
import Following from "./src/screens/Following";
import { Util } from "./src/Utils";

const screens = [
    { name: "Home", component: Home },
    { name: "Test", component: Test },
    { name: "Subscribe", component: Subscribe },
    { name: "PreviewLive", component: PreviewLive },
    { name: "SettingLive", component: SettingLive },
    { name: "Block", component: Block },
    { name: "Follower", component: Follower },
    { name: "Following", component: Following },
]

const Stack = createNativeStackNavigator();

function App() {
    return (
        <>
            <Provider store={store}>
                <I18nextProvider i18n={i18next}>
                    <Hooks />
                    <NavigationContainer>
                        <Stack.Navigator>
                            {screens.map(item => (
                                <Stack.Screen
                                    key={item.name}
                                    name={item.name}
                                    component={item.component}
                                    options={{ headerShown: false }} />
                            ))}
                        </Stack.Navigator>
                    </NavigationContainer>
                    {/* <Toast />
                    <Loading /> */}
                    <Util />
                </I18nextProvider>
            </Provider>
        </>
    );
}

export default App