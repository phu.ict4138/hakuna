import express from "express";

import configServer from "./src/config/ConfigServer.js";
import { config } from "./src/config/ConfigServer.js";
import connectDatabase from "./src/config/ConnectDatabase.js";
import initApiRouter from "./src/router/app/router.js";
import initModel from "./src/model/Model.js";

const app = express();
const port = config.port;

configServer(app);
connectDatabase();
initModel();


app.get("/", (req, res) => {
    return res.send("Hello world from Nodejs");
});

app.listen(port, () => {
    console.log(`Server is running at port ${port}`);
});

initApiRouter(app);
