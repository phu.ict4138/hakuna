import express from "express";
import cors from "cors";

const configServer = (app) => {
    app.use(express.json());
    app.use(express.urlencoded({
        extended: true
    }));
    app.use(cors());
    app.use(express.static("./"));
}

const config = {
    port: 8085,
    host: "localhost",
    database: "hakuna",
    username: "root",
    password: null
}

export default configServer
export {
    config
}