import { Sequelize } from "sequelize";

import { config } from "./ConfigServer.js";

const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: "mysql",
    logging: false,
});

const connectDatabase = async () => {
    try {
        await sequelize.authenticate();
        console.log('Connect database successfully!');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

export default connectDatabase
export { sequelize }