import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const AvatarImage = sequelize.define("avatar.image", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    idUser: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "id_user"
    },
    link: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default AvatarImage