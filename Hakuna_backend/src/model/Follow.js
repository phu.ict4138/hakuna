import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const Follow = sequelize.define("Follow", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    idUserFirst: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_user_first"
    },
    idUserSecond: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_user_second"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default Follow