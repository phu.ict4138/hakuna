import AvatarSetting from "./AccountSetting.js";
import AvatarImage from "./AvatarImage.js";
import Country from "./Country.js";
import Crown from "./Crown.js";
import Follow from "./Follow.js";
import Team from "./Team.js";
import TeamLogo from "./TeamLogo.js";
import Tier from "./Tier.js";
import User from "./User.js";

const initModel = async () => {
    if (
        !AvatarSetting ||
        !AvatarImage ||
        !Country ||
        !Crown ||
        !Follow ||
        !Team ||
        !TeamLogo ||
        !Tier ||
        !User) {
        return
    }
    return 1
}

export default initModel