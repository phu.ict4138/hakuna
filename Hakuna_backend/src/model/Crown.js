import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const Crown = sequelize.define("crown", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true
    },
    requiredExperience: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "required_experience"
    },
    imageSmall: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "image_small"
    },
    imageMedium: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "image_medium"
    },
    imageBig: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "image_small"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default Crown