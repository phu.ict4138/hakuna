import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const Tier = sequelize.define("tier", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true
    },
    type: {
        type: DataTypes.STRING,
        allowNull: true
    },
    startLevel: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "start_level"
    },
    endLevel: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "end_level"
    },
    idCrown: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_crown"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default Tier