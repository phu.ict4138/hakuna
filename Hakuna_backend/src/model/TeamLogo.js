import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const TeamLogo = sequelize.define("team.logo", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true
    },
    image: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "image"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default TeamLogo