import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const User = sequelize.define("user", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    idHakuna: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "id_hakuna",
        defaultValue: "000000000000"
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
    },
    birthday: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date()
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    experience: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    star: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    totalStar: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
        field: "total_star"
    },
    diamond: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    totalDiamond: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
        field: "total_diamond"
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    gender: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "UNDEFINE"
    },
    avatar: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "UNDEFINE"
    },
    idCountry: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_country"
    },
    idGroup: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_group"
    },
    idCrown: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_crown"
    },
    chatHighlight: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        field: "chat_highlight"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "update_at"
    },
    online: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    test: {
        type: DataTypes.INTEGER,
        allowNull: false,
    }
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default User