import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const Team = sequelize.define("team", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true
    },
    idLogo: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: "id_logo"
    },
    point: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    teamPlus: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
        field: "team_plus"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default Team