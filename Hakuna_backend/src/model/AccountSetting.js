import { DataTypes } from "sequelize";

import { sequelize } from "../config/ConnectDatabase.js";

const AvatarSetting = sequelize.define("avatar.setting", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    idUser: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "id_user"
    },
    allowNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "allow_notify"
    },
    newPostNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "new_post_notify"
    },
    likeAtBroadNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "like_at_broad_notify"
    },
    commentAtBroadNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "comment_at_broad_notify"
    },
    responseCommentNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "response_comment_notify"
    },
    livePublicNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "live_public_notify"
    },
    livePrivateNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "live_private_notify"
    },
    inviteFromFollowingNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "invite_from_following_notify"
    },
    inviteFromNotFollowingNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "invite_from_not_following_notify"
    },
    postFromFollowingNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "post_from_following_notify"
    },
    followingNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "following_notify"
    },
    pkFromFollowingNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "pk_from_following_notify"
    },
    groupInviteNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "group_invite_notify"
    },
    groupActivityNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "group_activity_notify"
    },
    levelNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "level_notify"
    },
    rankNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "rank_notify"
    },
    eventNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "event_notify"
    },
    receiveGiftNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "receive_gift_notify"
    },
    messageNotify: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "message_notify"
    },
    autoExchangeDiamond: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "auto_exchange_diamond"
    },
    allowReceiveGift: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "allow_receive_gift"
    },
    allowPostFanBoard: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
        field: "allow_post_fan_board"
    },
    createAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "create_at"
    },
    updateAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
        field: "update_at"
    },
}, {
    freezeTableName: true,
    createdAt: false,
    updatedAt: false
});

await sequelize.sync({ force: false });

export default AvatarSetting