import express from "express";

import {
    getAllUser,
    register
} from "../../controller/account.js";

const router = express.Router();

const accountRouter = (app) => {

    router.get("/", getAllUser);
    router.post("/register", register);

    return app.use("/api/v1/account", router);
}

export default accountRouter