
import accountRouter from "./account.js"

const initApiRouter = (app) => {
    accountRouter(app);
}

export default initApiRouter